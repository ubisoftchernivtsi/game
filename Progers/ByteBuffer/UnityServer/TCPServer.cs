﻿using System;
using System.Net;
using System.Net.Sockets;

namespace UnityServer
{
    public enum ServerPackets
    {
        S_INFORMATION = 1,
        S_EXECUTEMETHODONCLIENT,
    }

    class TCPServer
    {
        private TcpListener serverSocket;

        public Clients[] Client = new Clients[1500];  // Player Slots

        public void InitNetwork()
        {
            for (int i = 0; i < Client.Length; i++)
            {
                Client[i] = new Clients();
            }
            serverSocket = new TcpListener(IPAddress.Any, 5555);
            serverSocket.Start();
            serverSocket.BeginAcceptTcpClient(ClientConnectCallback, null);
        }

        private void ClientConnectCallback(IAsyncResult result)
        {
            TcpClient tempClient = serverSocket.EndAcceptTcpClient(result);
            serverSocket.BeginAcceptTcpClient(ClientConnectCallback, null);

            for (int i = 0; i < Client.Length; i++)
            {
                if (Client[i].socket == null)
                {
                    Client[i].socket = tempClient;
                    Client[i].connectionID = i;
                    Client[i].ip = tempClient.Client.RemoteEndPoint.ToString();
                    Client[i].Start();
                    Console.WriteLine("Connection recieved from " + Client[i].ip + ".");
                    return;
                }
            }
        }

        public void SendDataTo(int connectionID,byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteLong((data.GetUpperBound(0) - data.GetLowerBound(0))+1);
            buffer.WriteBytes(data);
            Client[connectionID].myStream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
        }

        //SEND METHODS TO CLIENT
        public void SendInformation(int connectionID)
        {
            ByteBuffer buffer = new ByteBuffer(); //creating new packet
            //add the packet identifier

            buffer.WriteLong((long)ServerPackets.S_INFORMATION);
            //adding data to packet

            buffer.WriteString("Welcom to SERVER!!!!!!");
            buffer.WriteString("You may be able to play the gaem!");
            buffer.WriteInteger(228);

            SendDataTo(connectionID, buffer.ToArray());
        }

        public void SendExecuteMethodOnClient(int connectionID)
        {
            ByteBuffer buffer = new ByteBuffer();
            //identifier

            buffer.WriteLong((long)ServerPackets.S_EXECUTEMETHODONCLIENT);
            SendDataTo(connectionID, buffer.ToArray());

        }
    }
}
