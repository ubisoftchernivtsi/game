﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientManager : MonoBehaviour {

    private ClientTCP clientTCP;
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
        clientTCP = new ClientTCP();
        clientTCP.Connect();
		
	}
	
    private void OnApplicationQuit()
    {
        clientTCP.client.Close();
    }

	// Update is called once per frame
	void Update () {
		
	}
}
