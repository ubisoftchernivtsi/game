﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;


public class ClientTCP
{

    public TcpClient client;
    public NetworkStream myStream;
    private byte[] asyncBuffer;
    public bool isConnected;

    private string IP_ADDRESS = "127.0.0.1";
    private int PORT = 5555;

    public void Connect()
    {
        Debug.Log("Connecting...");
        client = new TcpClient();
        client.ReceiveBufferSize = 4096;
        client.SendBufferSize = 4096;
        asyncBuffer = new byte[8192]; //why?
        try
        {
            client.BeginConnect(IP_ADDRESS, PORT, new AsyncCallback(ConnectCallback), client);
        }
        catch
        {
            Debug.Log("Could not connect to server!.");
        }
    }

    private void ConnectCallback(IAsyncResult result)
    {
        try
        {
            client.EndConnect(result);
            if (client.Connected == false) { Debug.Log("Could not connect to server!."); return; }
            else
            {
                myStream = client.GetStream();
                myStream.BeginRead(asyncBuffer, 0, 8192, OnRecieveData, null);
                isConnected = true;
                Debug.Log("You are CONNECTED!");
            }
        }
        catch (Exception)
        {

            isConnected = false;
            return;
        }
    }

    private void OnRecieveData(IAsyncResult result)
    {
        try
        {

        }
        catch (Exception)
        {

            throw;
        }
    }
}

